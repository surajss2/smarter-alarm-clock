\documentclass[letterpaper, 11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{mathptmx}
\usepackage[citestyle=authoryear,bibstyle=authortitle]{biblatex}
\addbibresource{main.bib}
\usepackage[top=1in, bottom=1.25in, left=1.25in, right=1.25in]{geometry}
\usepackage{enumitem}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hyperref}
\hypersetup{colorlinks=true,allcolors=MidnightBlue,pdfauthor={surajss2}}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{array}
\usepackage{booktabs}
\usepackage{comment}
\usepackage{csquotes}
\usepackage{microtype}
\usepackage[ragged]{footmisc}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}

\title{CS 437: Final Lab — Smarter Alarm Clock}
\author{Suraj Singh \\ NetId: surajss2 }

\begin{document}

\maketitle

\section{Project Links}\label{sec:links}
\begin{itemize}
    \item \href{https://mediaspace.illinois.edu/media/t/1_45bgmb6w}{Video: https://mediaspace.illinois.edu/media/t/1\_45bgmb6w}
    \item \href{https://gitlab.engr.illinois.edu/surajss2/smarter-alarm-clock}{GitLab Repository: https://gitlab.engr.illinois.edu/surajss2/smarter-alarm-clock}
\end{itemize}

\section{Introduction}\label{sec:intro}

\subsection{Topic}\label{sec:topic}
The goal for this project is to create an alarm clock that takes into account a user's sleep cycle data, which allows the alarm to more effectively wake up the user closer to moments of wakefulness.

\subsection{Motivation}\label{sec:motive}
Sleep is an essential activity we ideally spend on every night. 
Every time one sleeps, one cycles through 3 main stages of sleep (light, deep, and rapid-eye movement (REM)), each of which contributes to maintaining their physical and mental health.
However, with the multitude of attention and stresses that occur in modern life, it can be hard to get adequate sleep.
This can lead to a build up of sleep debt, which, as stated by~\cite{luyster2012sleep}, significantly impairs cognitive and motor functions, as well as increasing disease and accident risk, decreasing longevity, and increased individual and societal costs (e.g., healthcare).
There are many avenues for tackling the problem of sleep deprivation through technology, from habit development apps to blocking app that stops the user from using their smart devices after a set time.
The pathway I choose to focus on is augmenting the capabilities of alarm clocks.

The alarm clock itself is a simple device that is designed to wake up the user at a set time.
Within this simplicity there is a problem though, which is that alarm clocks have no idea what stage of sleep the user is in when the alarm goes off.
This can cause the alarm to go off during inopportune times, which can lead to the user feeling groggy, snoozing the alarm, or at worst, not even waking up.
A way to fix this issue would be to have the alarm clock contain a mechanism for determining where user is along the sleep cycle.
This can allow the alarm to more gently wake the user up and can help them rely less on snoozing the alarm.

Many people have fitness devices that are able to measure sleep such as Fitbit watch, a SleepScore on-table device, or a Withing under-mattress device.
According to~\cite{10.1093/sleep/zsaa291}, consumer level sleep trackers on average do very well with detecting wakefulness and sleep, and can sometime achieve results as good as professional devices.
However, they are mainly able to achieve this with consistent sleep results based on user information, and may not be as accurate for nights with poor or abnormal sleep.
Even so, the relative information provided by these devices can be used to tailor models to the individual's sleep patterns. 

There have been applications that have applied similar the one I am describing, namely Sleep Cycle for iOS and Android.
The way the Sleep Cycle application works is by having the user set a time window range to wake up, rather than just a single time point.
The application will then guess based on sound and movement approximately what sleep stage the user is in and then try to wake up the user at the best possible time within that time window.
My application will take this idea and focus more on bringing this functionality to non-smartphone devices by using the Withing Sleep Analyzer.

\section{Technical Approach}\label{sec:technical}
The overall approach to the project can be split into two primary parts: the alarm clock and the AI model.
In addition, I will also discuss how the data used for model training.

\subsection{Data}\label{sec:data}
For the data in this project, I used sleep data provided to me by a participant who uses the Withing Sleep Mat.
The Withing Sleep Mat Analyzer, is an under-mattress device that takes measurements using air pressure to measure user movement (such as respiration) and electronic sensor to measure heart rate.
This is then used to to calcualte the person's sleep time, sleep efficiency, and approximate sleep stages.
According to~\cite{Edouard2021}, the Withing Sleep Analyzer does very well with time to sleep against a traditional polygraphy device, which is useful for the project as this is the primary method used to determine when to start the alarm.
I extracted the data as a CSV file from the Withing website, and it contained slightly over 3 years of data, with row in the data specifying a sleep event (in UTC time), the stages determined, and the duration.
According to~\cite{strathprints79694}, one needs to have over 2 months of sleep data to account for variability in sleep (such as due to daylight saving time, seasonality, and outlier periods of sleep), so having this much data is very useful for determining the user's consistent sleep pattern.
There was also extra data on heart rate and respiration for the sleep time, however I did not use them for this project.
I will discuss more about cleaning and reformatting the data in Section~\ref{sec:data-impl}.

\subsection{Alarm Clock}\label{sec:alarm}
I modeled the alarm clock as a Finite State Machine (FSM) [see Figure~\ref{fig:fsm} for diagram] with the following states:
\begin{enumerate}
    \item SET:\@the alarm clock is set, but not started (useful for delayed start),
    \item RUNNING:\@the alarm clock is started and running, will run the alarm when the specified time is reached,
    \item PLAYING:\@the alarm clock is playing the alarm (derived classes will override this with different ways of activating the alarm),
    \item SNOOZING:\@the alarm clock is temporarily stopped (will play again after some time),
    \item DEACTIVATED:\@the alarm clock is fully turned off.
\end{enumerate}
The transition between the states would be:
\begin{itemize}
    \item Set Alarm: DEACTIVATED $-->$ SET
    \item Start Alarm: SET $-->$ RUNNING
    \item Sound Alarm: RUNNING $-->$ PLAYING
    \item Snooze Alarm: PLAYING $-->$ SNOOZING
    \item Un-snooze Alarm: SNOOZING $-->$ PLAYING
    \item Stop Alarm: RUNNING $-->$ DEACTIVATED $or$ RUNNING $-->$ SET
\end{itemize}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Images/mermaid-diagram-20220515142030.png}
    \caption{Finite State Diagram of the Alarm Clock.}\label{fig:fsm}
\end{figure}
Using this structure allowed me to modularize the code for the alarm clock and easily slot extra elements such as audio using a VLC instance and the AI model.

\subsection{AI Model}\label{sec:model}
During the consideration process, I looked at Feed-Forward Networks, Convolution Neural Networks (CNNs), and Recurrent Neural Networks (RNNs), as well as some non-neural network models such as Markov Chains.
Each of the models considered required taking a set range of sleep data (with prior probabilities), running calculations, and then return new probabilities for the next time step.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/Memory-Model.png}
    \caption{An example of how the data flows between memory model and predictor model. Squares are the data (probabilities) and circles are nodes. Red squares are old data, yellow is current data, and green is next data. Red circle is the input layer units, yellow is the hidden layer units, and green is the output layer units.}\label{fig:model}
\end{figure}
Since I could not stream the data from the Withing Sleep Analyzer while the person was sleeping, each model needed to be able to independently calculate and hold on to their own probabilities.
This meant having a separate data structure to hold the model's `memory'.
With this, the flow of the data would be as follows:
\begin{enumerate}
    \item Initialize the model's memory, containing previous time steps and predicted probabilities,
    \item Pass the current memory and time step to the model's function and calculate the next prediction,
    \item Append the next time step and the newly predicted probabilities to the model's memory
    \item Repeat back to the step 2 until the program has stopped.
\end{enumerate}
[see Figure~\ref{fig:model} for visualization]

Given this, I settled on a simple Feed-Forward network for the baseline model and on using some variations of RNNs and Attention Mechanism for the main model.
I chose the Feed-Forward network as a baseline as its structure closely matches the other RNN models, allowing for easier comparisons.
I chose Recurrence neural network models for the general models as they contain neural units that allowed holding information about prior inputs and states, which is useful for time-series data such as sleep cycle.
For the RNN variants, I focused on Long Short-Term Memory (LSTM) model and Gated Recurrent Unit (GRU) model, both of which fix the exploding/vanishing gradient problem through the use of `gated' units to hold on to prior information.
I also looked at Attention based models, which uses contextual states to emphasize certain parts of the data and de-emphasize other parts.

\section{Implementation Details}\label{sec:implementation}
Full implementation available at the GitLab repository (see Section~\ref{sec:links}).
Below is a list of all directly-imported external Python libraries I used in the project:
\begin{enumerate}
    \item \href{https://github.com/tensorflow/tensorflow}{Tensorflow}: Library for building and developing the neural network models. Provided under the Apache 2.0 License.
    \item \href{https://github.com/philipperemy/keras-attention-mechanism}{Keras-Attention-Mechanism}: An attention-mechanism layer implemented in Keras. Provided under the Apache 2.0 License.
    \item \href{https://github.com/numpy/numpy}{Numpy}: Math library, also used for passing data to neural models. Provided under the BSD-3-Clause License.
    \item \href{https://github.com/pandas-dev/pandas}{Pandas}: Data frame library. Provided under the BSD-3-Clause License.
    \item \href{https://github.com/jupyter/notebook}{Jupyter}: Interactive Python notebook. Provided under the BSD-3-Clause License.
    \item \href{https://github.com/moses-palmer/pynput}{Pynput}/\href{https://github.com/boppreh/keyboard}{Keyboard}: Provides keyboard input callback in Python. Pynput used for Mac and Keyboard used for Raspberry Pi (Linux). Pynput provided under GNU Lesser General Public License v3.0 and Keyboard provided under MIT License.
    \item \href{https://pypi.org/project/python-vlc/}{Python-VLC}: Allows running VLC from Python. Python-VLC provided under GNU Lesser General Public License v2.0 License.
\end{enumerate}
On the hardware, I did primary development and testing on my MacBook Pro Laptop.
I also attempted to get this project working on the Raspberry Pi, but I ran into some issues discussed more in Section~\ref{sec:issue}.

\subsection{Sleep Data}\label{sec:data-impl}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/ConfusionMatrix.png}
    \caption{A confusion matrix between Withing Sleep Analyzer and EEG device; provided by~\cite{WithingReview}.}\label{fig:matrix}
\end{figure}
Before cleaning the data, the raw sleep data contained only the sleep start value (date-time value), the sleep stages (a list of integers as enumerations of the sleep stage) and duration (list of integers as seconds).
During the cleanup process, I mapped each pair of lists into an individual row.
Each row has a start and end time (as date-time value), the minutes since the start of entry (as integer), and expanded the sleep stage data into a one-hot probability table (each stage is a float between 0.0 and 1.0 with only one column containing the 1.0 and the rest being 0.0).
An additional step I took was adding random noise to the sleep data using a multinomial distribution with probability data taken from a confusion matrix (see Figure~\ref{fig:matrix}).
This allowed each of the models to be more resilient to noisy data, especially since the data will be coming from their own estimation.
After organizing the data, I have a class called WindowGenerator that splits the data into training, validation, and testing datasets, as well as pushing the data into a time-series format, where the input is a specified chunk of time, and the target value is the next time step.

\subsection{Alarm Clock Class}\label{sec:alarm-impl}
For the alarm clock implementation, I created three scripts: \href{https://gitlab.engr.illinois.edu/surajss2/smarter-alarm-clock/-/blob/main/alarm.py}{alarm.py}, \href{https://gitlab.engr.illinois.edu/surajss2/smarter-alarm-clock/-/blob/main/simple_alarm_clock.py}{simple\_alarm\_clock.py}, and \href{https://gitlab.engr.illinois.edu/surajss2/smarter-alarm-clock/-/blob/main/smart_alarm_clock.py}{smart\_alarm\_clock.py}.
The alarm.py, contains the base FSM for the alarm clock, which built up from two Python classes: Alarm State enum for the 5 states of the FSM and BaseAlarmClock, which implements the state changing functionality and holds the reference to the wake-up time.
Next, the SimpleAlarmClock in simple\_alarm\_clock.py extends the BaseAlarmClock, adding functionality for keyboard inputs, providing print messages for alarm clock state changes, and promoting the user for information about what time to wake up. 
It also adds a function called `simple\_alarm\_mode', which handles running the alarm clock from start to end.
Moving into the smart\_alarm\_clock.py script, the code structure for is kept mostly the same as simple\_alarm\_clock.py, principally the user input and the flow of the data from alarm activation to deactivation.
The three major additions for this script are: 
\begin{enumerate}
    \item a function for loading and running the model (includes the softmax and model prediction helper functions),
    \item a deque (Double-Ended Queue) to represent the model's memory (set to a max-len of 5 [in minutes]),
    \item and a VLC instance for playing sound from the script (this allowed it to sound like a regular alarm clock).
\end{enumerate}

\subsection{AI Models}\label{sec:model-impl}

For implementing the models, I first started by experimenting in a Jupyter Notebook to create the models and play around with setting up the parameters.
Since each model had only a single hidden layer, the primary hyperparameter to tune for all the models was the number of units in the hidden layer.
To control for variations in the data, especially the randomization of the input probabilities, I made sure to save the data I generated into a separate CSV file and load the same information for each model. 
The input layer is a batched 5 by 8 matrix representing 5 last known time step and their associated 4-stages (awake, REM, light sleep, and deep sleep) probabilities.
The output layer contains 4 output units for each stage's probability.
These are the specific models, defined with layer(units), I trained with their final parameter amounts:
\begin{itemize}
    \item Input(5,8) $=>$ Dense(32) $=>$ Flatten $=>$ Dense(4): 900 training parameters,
    \item Input(5,8) $=>$ LSTM(16) $=>$ Dense(4): 1604 training parameters,
    \item Input(5,8) $=>$ GRU(16) $=>$ Dense(4): 1268 training parameters,
    \item Input(5,8) $=>$ Attention(32) $=>$ Dense(4): 629 training parameters.
\end{itemize}

Once I finished experimenting with each of the models, I pushed them into their own Python script to allow training to be more parallelized.
Each training script initialized the training data and a specific model, with it then fitting the model to the data, and finally saving a training+validation history log and trained model.
As a check, I also evaluate it with the withheld testing data to verify the model did not overfit.
I also added a softmax function at the end of each of the models to yield more reasonable probabilities.

\section{Results}\label{sec:results}
\subsection{Outcomes}\label{sec:outcome}
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Images/Base-History.png}
        \caption{Baseline model Log Loss over training iterations.}\label{fig:base}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Images/LSTM-History.png}
        \caption{LSTM model Log Loss over training iterations.}\label{fig:lstm}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Images/GRU-History.png}
        \caption{GRU model Log Loss over training iterations.}\label{fig:gru}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Images/Attention-History.png}
        \caption{Attention model Log Loss over training iterations.}\label{fig:attn}
    \end{subfigure}
    \caption{Log Loss over training iterations for each of the 4 models trained. Note the exponential decay of each training data, likely due to the data pattern being very simple to learn.}\label{fig:model-graphs}
\end{figure}
For the full data, the training-validation-testing split was 40\%, 30\%, 30\% of the randomly sampled data.
Since this is a time series data, I made sure that any time window that occurred on the same day appeared in only a single dataset (e.g., a day cannot be represented in both the training and validation dataset).
This made sure the models got an accurate subset of the data without missing pieces.

During training, all the models to initially perform too well, as every model, including the baseline, reached over 95\% accuracy.
This is likely due to the very simple nature of the current data, as it only has 8 inputs per time step, with only 4 of them being probabilities. 
This also lead the models to overfit with it favoring being awake over all the other sleep stages.
I switch from categorical accuracy to categorical cross-entropy, which forced the model to take more clear stances on which sleep stage the user is in as it measures the residual between predicted and target, rather than how many  it got correct.
This still had some overfitting issues, but was able to have more stable probabilities, especially within data in the middle of the night.
After training each model on the data, all the models were very close in terms of categorical loss entropy (see Figure~\ref{fig:model-graphs}).
As there was no stand-out best model, I took the attention mechanism model, as it was the fastest to train and the most lightweight compare to the current trained RNN-based models.

Moving onto the alarm clock script, setup was simple enough for each of the 3 script and I ran into no major issues with when I ran it on my Mac laptop.
Running the simple\_alarm\_clock script, I was able to validate that the finite state machine was working and properly changing state based on the specific conditions layout in Section~\ref{sec:alarm}.
For the smart\_alarm\_clock script, I was able to verify that the model was able to process the input and store the outputs correctly, and that the script was able to play the song when the alarm hit the wake state.

\subsection{Issues}\label{sec:issue}
One issue I ran into for the AI model was the inability for the model to work load properly on the Raspberry Pi. 
My first attempt was to load the model directly from using TensorFlow.
This failed to work on the Raspberry Pi since the most recent available version of TensorFlow for the Pi (Linux ARM) is version 1.14.
To work around that problem, I looked at converting the model to TensorFlow Lite (TFLite).
This was promising, since there was no problem during the conversion process nor during the loading process.
However, when I attempted to run the model, it would lead to a `Segmentation Fault'.
I verified that the model inputs and outputs were correct, so my best guess of what the issue is that there may be an issue converting Keras models into TFLite models.
I viewed these two issues on TensorFlow's GitHub Issues board \href{https://github.com/tensorflow/tensorflow/issues/29794}{Convert Keras to tflite \#29794} and \href{https://github.com/tensorflow/tensorflow/issues/32849}{TFlite conversion of tf.keras model fails \#32849} in an attempt to solve this problem, but was not able to fix it within this time period.

Another issue I experienced with the Raspberry Pi was with the PulseAudio not authorizing for the VLC library.
During the start-up of the application, I would initialize a VLC instance and load in the song into the instance.
However, the VLC instance fails to load in properly with the error message being `PulseAudio: Unable to connect: Connection refused'.
Running the script in `sudo' mode did not help  fix this issue nor did resetting the PulseAudio daemon. 
This did not seem to be an issue with the Raspberry Pi's sound system itself, since the terminal command `omxplayer' was able to load and play the sound to the 3.5 mm headphone port.
I did try to see if running OMXPlayer from the Python subprocess library would work, but I was unable to have it close during the snoozing and stopping states without fully terminating the program.

These issues prevented me from running the working application on the Raspberry Pi.
Had I more time to debug the issues, I may have gotten the TF model and VLC player to work on the Pi.

\subsection{Takeaways}\label{sec:takeaway}
Some key takeaways I had from this project:
\begin{itemize}
    \item Data may not necessarily be in the correct format, so cleaning and reorganizing may be required. 
    \item Allow the data to be randomized to make models more resistant to noise, especially if that is fed back into the model.
    \item Simple models can be just as effective as more complex models, especially if the data is simple.
    \item Code can be kept modular by pulling out common patterns into their own script. This can make it easy to include additional functionality.
    \item Work on machines as close to the target device as possible, to make debugging issues easier to manage.
\end{itemize}

\subsection{Future Work}\label{sec:future}
In this final discussion, we will look at where the project can go from here.
Given additional time, the first priority would be on getting the application to successfully work from start to end on the Raspberry Pi.
The tools and scripts are there for it to work, with the current issue just being able to fix bugs during the setup process.
The next extension on that would be to connect up the Withing API using Withing Python.
While there isn't a way to stream sleep data as it is being collected, I can still use it to gather the sleep data after the user wakes up, which can allow the model to be trained on new data daily.
Additionally, I can also hook into the notification system provided by the API for user in-bed and user out-of-bed to automate starting and stopping the alarm.
Extending to a more general idea, there might be ways to add in other devices to provide sleep data tracking, such as FitBit or Sleep Cycle, to provide more supported devices for evaluation user sleep data.
In all, this project has hopefully shown that an offline and smartphone-less can be useful endeavor for improving alarm clocks.

\printbibliography

\end{document}

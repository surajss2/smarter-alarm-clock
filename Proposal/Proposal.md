# Project Proposal

Suraj Singh (surajss2)

## Project Topic

Create a smarter alarm clock that is able to learn from my sleep pattern and determine the best time to wake me up in a specific window with the lowest chance of being snoozed. Specifically, I will use the Withing Sleep Mat to notify when I go to bed, run an neural network to approximate the stage of sleep I am in, and run a Python script alarm from the Raspberry Pi when a good time window matches.

## Motivation

Sleep is an important part of life, but with busy schedules and many distractions, it can be hard to manage healthy sleep hygiene. Snoozing one's alarm clock can often be used to give just a little bit more sleep, but the problem is that it create a bad habit of ignoring the alarms. Another issues with traditional alarm clocks they are essentially closed devices, where they have no way of taking information from outside sources. An example would be one's sleep pattern, which can provide useful feedback about what stage of sleep they are in. Being woken up up from deep sleep can lead to feelings of grogginess, and that is if the alarm is heard. The idea for a smarter alarm clock would be to address these shortcomings while also being simple enough to use. 

## Timeline

For this project, I will work towards 2 main deliverables:

1. An AI trained on my sleep data,
2. An alarm clock Python script run on the Raspberry Pi.

With the target of around 25 hours, here is approximately the timeline for the project:

* Week 1 (04/04 to 04/10): ~6 hours
  1. Download my sleep data from the Withing website and do some analysis of the structure of the data.
  2. Create a simple alarm clock in Python that runs on the Raspberry Pi. The minimum functionality is to trigger an alarm sound at a specific time.

* Week 2 (04/11 to 04/17): ~7 hours
  1. Preprocess the sleep data to prepare it for training. Primarily split the data into training and validation datasets.
  2. Setup the alarm system with the Withing API to receive notification of sleep events. Some examples would be user is in bed and user has left the bed.

* Week 3 (04/18 to 04/24): ~5 hours
  1. Training a model with the sleep data and test with my most recent sleep data.
  2. Soft test run of the Raspberry Pi alarm clock: start timer when in bed notification is triggered and run alarm when out of bed notification is triggered.

* Week 4 (04/25 to 05/01): ~4 hours
  1. Connect AI model to the Pi and runs tests to see how well it works. Test will include if it can pickup sleep events from the Withing device, how accurate the AI model guesses the sleep stage, and if it is able to trigger the alarm at a reasonable time.

* Week 5 (05/02 to 05/08): ~3 hours
  1. Polishing up the model and Python scripts.
  2. Finalize video and report.

* Some pieces of hardware and software I will use for this project will include:
    * Hardware:
        * Raspberry Pi
        * (Bluetooth) Speaker
    * Software:
        * Python
        * Withing API: https://developer.withings.com/api-reference#tag/sleep
        * Tensorflow Lite or PyTorch Mobile for Deep Learning Neural Network model

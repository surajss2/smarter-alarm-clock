from common import *

# Hyper-parameters
GRU_UNITS = 16

# Model Definition
def main(baseline=None):
    gru_model = keras.Sequential()
    gru_model.add(layers.Input(shape=(INPUT_TIME_STEP, INPUT_FEATURES_SIZE)))
    gru_model.add(layers.GRU(GRU_UNITS))
    gru_model.add(layers.Dense(SLEEP_STAGES))
    gru_model.build()
    print(gru_model.summary())
    gru_history = compile_and_fit(model=gru_model, window=wg, baseline=baseline)
    print("Finished training")
    gru_model.save(".model/gru")
    save_history(gru_history.history, ".history/gru.csv")

if __name__ == "__main__":
    main()
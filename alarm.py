from enum import Enum, auto
from datetime import datetime
from typing import Optional


class AlarmState(Enum):
    SET = auto(),
    RUNNING = auto(),
    PLAYING = auto(),
    SNOOZED = auto(),
    DEACTIVATED = auto(),


class BaseAlarmClock:
    _current_state: AlarmState
    _wake_time: datetime

    def __init__(self, wake_time: Optional[datetime] = None):
        self._wake_time = wake_time if wake_time else datetime.utcnow()
        self._current_state = AlarmState.DEACTIVATED

    @property
    def current_state(self):
        return self._current_state

    @property
    def wake_time(self):
        return self._wake_time

    def set_alarm(self, wake_time: datetime) -> datetime:
        self._current_state = AlarmState.SET
        self._wake_time = wake_time
        return self._wake_time

    def start_alarm(self) -> None:
        self._current_state = AlarmState.RUNNING

    def sound_alarm(self) -> None:
        self._current_state = AlarmState.PLAYING

    def snooze_alarm(self) -> None:
        self._current_state = AlarmState.SNOOZED

    def stop_alarm(self, deactivate: bool = True) -> None:
        self._current_state = AlarmState.DEACTIVATED if deactivate else AlarmState.SET

    def alarm_check_reached(self, current_time: datetime) -> bool:
        return current_time >= self._wake_time if self._current_state is AlarmState.RUNNING else False
